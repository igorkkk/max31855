local M = {}
local SSPin
 function M.setup(config)
	local config = config or {}
	SSPin = assert(config.SSPin, 'Lost Slave Select Pin' )
	spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 8, 8)
	gpio.mode(SSPin, gpio.OUTPUT)
	gpio.write(SSPin, gpio.HIGH)
end
function M.gettmp(ttable)
	if not SSpin then
		M.setup({SSPin = 8})
	end
	if not ttable then 
		ttable = {} 
		print('Lost Table!')
	end
	local dat, mb, lb, t
	gpio.write(SSPin, gpio.LOW)
	dat = (spi.recv(1, 4))
	gpio.write(SSPin, gpio.HIGH)
	mb = string.byte(string.sub(dat, 3,3)) 
	lb = string.byte(string.sub(dat, 4,4))
	t = lb + mb * 256
	t = bit.rshift(t, 4)
	if t >= 2048 then t = t - 4096 end
	t = t*0.0625
	print('Internal:', t)
	ttable.intemp = t
	mb = string.byte(string.sub(dat, 1,1)) 
	lb = string.byte(string.sub(dat, 2,2))
	t = lb + mb * 256
	t = bit.rshift(t, 2)
	if t >= 8192 then t = t - 16384 end
	t = t*0.25
	print('Thermocup:', t)
	ttable.tpair = t 
	dat, mb, lb, t = nil, nil, nil, nil
end
return M