local M = {}
function M.setup()
	spi.setup(1, spi.MASTER, spi.CPOL_LOW, spi.CPHA_LOW, 8, 8)
	gpio.mode(M.SSPin, gpio.OUTPUT); gpio.write(M.SSPin, gpio.HIGH)
	M.setup = nil
end
function M.gettmp(ttable, pin)
	local comtb = {
		{3,2048,0.0625,'intemp'},
		{1,8192,0.25,'tcup'}
	}
	if not M.SSPin then M.SSPin = pin or 8; M.setup() end
	if not ttable then ttable = {} print('Lost Table!')	end
	local dat, mb, t
	gpio.write(M.SSPin, gpio.LOW)
	dat = (spi.recv(1, 4))
	gpio.write(M.SSPin, gpio.HIGH)
	for i = 1, 2 do
		mb = string.byte(string.sub(dat, comtb[i][1],comtb[i][1])) 
		t = string.byte(string.sub(dat, comtb[i][1]+1,comtb[i][1]+1))
		t = t + mb * 256
		t = bit.rshift(t, comtb[i][1]+1)
		if t >= comtb[i][2] then t = t - comtb[i][2]*2 end
		t = t*comtb[i][3]
		print(comtb[i][4], t)
		ttable[comtb[i][4]] = t
	end
	dat, mb, t, comtb = nil,nil,nil,nil
end
return M